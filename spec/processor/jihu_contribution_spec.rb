# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/jihu_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::JiHuContribution do
  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        jihu_contributor?: false,
        noteable_path: '/moo'
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open']

  describe '#applicable?' do
    context 'when event is for a new merge request opened by a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event is not from a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts a message to apply the label' do
      expected_message =<<~MARKDOWN.chomp
        /label ~"JiHu contribution"
        cc @gitlab-com/gl-security/appsec this is a ~"JiHu contribution", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions)
      MARKDOWN

      expect_discussion_request(event: event, body: expected_message) do
        subject.process
      end
    end
  end
end
