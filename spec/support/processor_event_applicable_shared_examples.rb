# frozen_string_literal: true

RSpec.shared_examples 'event is not applicable' do
  it 'is not applicable' do
    expect(subject.applicable?).to be_falsey
  end

  it 'does not process the event' do
    expect(subject).not_to receive(:process)

    subject.triage
  end
end

RSpec.shared_examples 'event is applicable' do
  it 'is applicable' do
    expect(subject.applicable?).to be_truthy
  end
end
