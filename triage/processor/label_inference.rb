# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class LabelInference < Processor
    Label = Struct.new(:text, :multiple_parents) do
      alias_method :to_s, :text

      def multiple_parents
        self[:multiple_parents] || []
      end

      def has_multiple_parents?
        multiple_parents.any?
      end

      def ==(other_label)
        self.text == other_label.to_s
      end

      def scoped?
        text.end_with?('::')
      end

      def parent_of?(other_label)
        scoped? && other_label.to_s.start_with?(text)
      end

      def match?(other_label)
        parent_of?(other_label) || self == other_label
      end

      def multiple_parents_exclude?(labels)
        (multiple_parents & labels.map(&:text)).empty?
      end

      def to_markdown
        %Q(~"#{text}")
      end
    end

    LabelTree = Struct.new(:parent_label, :child_labels, :label_group) do
      def same_parent?(other_label_tree)
        parent_label.match?(other_label_tree.parent_label)
      end

      def same_label_group?(other_label_tree)
        return false if label_group.nil?

        other_label_tree.label_group == label_group
      end
    end

    LabelMatch = Struct.new(:label, :label_tree)

    LABELS_TAXONOMY = [
      LabelTree.new(Label.new('type::feature'), [
        Label.new('feature::')
      ],
      :mr_type),
      LabelTree.new(Label.new('type::maintenance'), [
        Label.new('maintenance::'),
        Label.new('security', %w[type::maintenance type::bug])
      ],
      :mr_type),
      LabelTree.new(Label.new('type::bug'), [
        Label.new('bug::')
      ],
      :mr_type),
      LabelTree.new(Label.new('type::tooling'), [
        Label.new('tooling::')
      ],
      :mr_type),
      LabelTree.new(Label.new('Engineering Productivity'), [
        Label.new('ep::')
      ]),
      LabelTree.new(Label.new('Engineering Allocation'), [
        Label.new('Eng-Consumer::'),
        Label.new('Eng-Producer::')
      ])
    ].freeze

    react_to 'issue.*', 'merge_request.*'

    def applicable?
      event.from_gitlab_org? &&
        child_labels_added? &&
        parent_labels_to_add?
    end

    def process
      update_parent_labels
    end

    private

    def child_labels_added?
      added_child_label_matches.any?
    end

    def parent_labels_to_add?
      current_parent_labels.empty? || parent_label_needs_to_be_changed?
    end

    def parent_label_needs_to_be_changed?
      added_child_label_matches.none? { |added_child_label_match| added_child_label_match.label.has_multiple_parents? } &&
        parent_labels_to_add.any?
    end

    def current_parent_labels
      @current_parent_labels ||= LABELS_TAXONOMY.select do |label_tree|
        event.label_names.include?(label_tree.parent_label.to_s)
      end.map(&:parent_label)
    end

    def added_child_label_matches
      @added_child_label_matches ||=
        event.added_label_names.each_with_object([]) do |label, memo|
          LABELS_TAXONOMY.each do |label_tree|
            label_tree.child_labels.each do |child_label|
              # We only detect one label per label group
              next if memo.any? { |label_match| label_tree.same_label_group?(label_match.label_tree) }

              memo << LabelMatch.new(child_label, label_tree) if child_label.match?(label)
            end
          end
        end
    end

    def parent_labels_to_add
      @parent_labels_to_add ||= added_child_label_matches
        .select { |added_child_label_match| added_child_label_match.label.multiple_parents_exclude?(current_parent_labels) }
        .map { |label| label.label_tree.parent_label } - current_parent_labels
    end

    def parent_labels_to_remove
      @parent_labels_to_remove ||= label_trees_in_same_label_groups(added_child_label_matches).map(&:parent_label).select do |parent_label|
        event.label_names.include?(parent_label.text)
      end
    end

    def label_trees_in_same_label_groups(label_matches)
      label_matches.flat_map do |label_match|
        LABELS_TAXONOMY.select do |label_tree|
          label_match.label.multiple_parents_exclude?([label_tree.parent_label]) &&
            !label_match.label_tree.same_parent?(label_tree) && label_match.label_tree.same_label_group?(label_tree)
        end
      end
    end

    def update_parent_labels
      comment = ''
      comment += "/label #{parent_labels_to_add.map(&:to_markdown).join(' ')}" if parent_labels_to_add.any?
      comment += "\n/unlabel #{parent_labels_to_remove.map(&:to_markdown).join(' ')}" if parent_labels_to_remove.any?

      add_comment(comment) unless comment.empty?
    end
  end
end
