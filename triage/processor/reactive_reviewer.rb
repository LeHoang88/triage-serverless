# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/rate_limit'

require 'digest'

module Triage
  class ReactiveReviewer < Processor
    include RateLimit

    react_to 'merge_request.note'

    def applicable?
      event.from_gitlab_org? &&
        event.by_noteable_author? &&
        event.jihu_contributor? &&
        valid_action_prefix? &&
        valid_reviewers?
    end

    def process
      post_assign_reviewer_command
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("reactive-reviewer-commands-sent-#{event.user['id']}")
    end

    private

    ACTION_PREFIX_REGEXP = /^#{Regexp.escape(GITLAB_BOT)}[[:space:]]+assign_reviewer[[:space:]]+/
    ACTION_REGEXP = /#{ACTION_PREFIX_REGEXP}.+/
    REVIEWERS_REGEX = /@([^@ ]+)/.freeze

    def valid_action_prefix?
      actions.any?
    end

    def actions
      @actions ||= event.new_comment.scan(ACTION_REGEXP)
    end

    def valid_reviewers?
      valid_reviewers.any?
    end

    def valid_reviewers
      @valid_reviewers ||= requested_reviewers & Triage.gitlab_org_group_member_usernames
    end

    def requested_reviewers
      @requested_reviewers ||= actions
        .flat_map { |line| line.sub(ACTION_PREFIX_REGEXP, '').scan(REVIEWERS_REGEX) }
        .flatten
        .compact
    end

    def post_assign_reviewer_command
      add_comment <<~MARKDOWN.chomp
        /assign_reviewer #{reviewer_mentions}
      MARKDOWN
    end

    def reviewer_mentions
      valid_reviewers.map { |reviewer| "@#{reviewer}"}.join(' ')
    end
  end
end
